@extends("crudbooster::admin_template")
@section("content")
  <a href="{{route('ProjectCreate')}}" class="btn btn-sm btn-success" style="margin-bottom:20px" title="Add Data">
    <i class="fa fa-plus-circle"></i> Add New Project
  </a>

<div class="row">
  @foreach($projects as $isi)
    <?php
      $user= DB::table('cms_users')->where('id',$isi->user_id)->first();
      $tododone = DB::table('todolist')->where('project_id',$isi->id)->where('job_status','done')->count();
      $todoopen = DB::table('todolist')->where('project_id',$isi->id)->count();
      $activites = DB::table('activities')->where('project_id',$isi->id)->count();
     ?>
  <div class="col-md-3">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-black" style="background: url('{{url('/')}}/uploads/{{$isi->project_img}}') center center;">
        
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="{{url('/')}}/{{$user->photo}}" alt="{{$user->name}}">
      </div>
      <div class="box-footer">
         <div class="row">
             <div class="col-sm-12">
                 <h3>{{$isi->nama_project}}</h3>
                 <p>Project by : {{$user->name}}</p>
             </div>
         </div>
        <div class="row">
          <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">{{$activites}}</h5>
                    <span class="description-text">UPDATES</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">{{$tododone}}/{{$todoopen}}</h5>
                    <span class="description-text">TO DO</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header">{{$isi->status}}</h5>
                    <div class="description-text">STATUS</div>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->

        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-md-12">
            @if ($isi->status=="active")
              <a href="{{route('ProjectDetail',$isi->id)}}" class="btn btn-block btn-success">Manage</a>
              @else
              <a href="{{route('ProjectDetail',$isi->id)}}" class="btn btn-block btn-default">View Detail</a>
            @endif
          </div>
        </div>
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
  @endforeach

</div>

@endsection


@section('calljs')

 <script src="{{url('/')}}/js/clipboard.min.js"></script>
@endsection

@section('jsonpage')
<script type="text/javascript">
$(function() {
  var clipboard = new Clipboard('.btn');
  clipboard.on('success', function(e) {
    swal({
      type: 'success',
      title: 'Text copied!',
      text: 'ctrl+v to paste',
      timer: 2000,
      showConfirmButton: false,
    })
  });
})
</script>
@endsection