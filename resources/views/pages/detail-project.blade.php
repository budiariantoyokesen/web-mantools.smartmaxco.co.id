@extends("crudbooster::admin_template")

@section('csslineup')
  <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.css">

@endsection

@section("content")
  <div class="row">
    <div class="col-md-6">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          {{$project->nama_project}}
        </div>
        <!-- /.box-body -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <img src="{{url('/')}}/uploads/{{$project->project_img}}" width="100%" alt="">
            </div>
            <div class="col-md-8">
              {!! nl2br($project->project_desc) !!}
            </div>
          </div>

        </div>
      </div>
      <!-- /.box -->




      <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">To Do List</h3>

              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  {{--
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                  --}}
                </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <ul class="todo-list">
                @foreach($todolist as $todo)
                  <?php
                    switch($todo->job_status){
                      case 'new' : $color = 'primary';break;
                      case 'progress' : $color = 'primary';break;
                      case 'check' : $color = 'warning';break;
                      case 'reject' : $color = 'danger';break;
                      case 'revision' : $color = 'warning';break;
                      case 'done' : $color = 'success';break;
                    }

                    $todo_user = DB::table('cms_users')->where('id',$todo->user_id)->first();
                  ?>
                  <li class="{{$todo->job_status == 'done' ? 'done' : ''}}" data-id="{{$todo->id}}" data-project_id="{{$todo->project_id}}" data-user_id="{{$todo->user_id}}">
                    <!-- drag handle -->
                    <span class="handle">
                          <i class="fa fa-ellipsis-v"></i>
                          <i class="fa fa-ellipsis-v"></i>
                        </span>
                    <!-- checkbox -->
                    @if($todo->job_status != 'done' && $todo->user_id == Crudbooster::myID())
                      <input type="checkbox"  onclick='window.location.assign("{{route('ProjectDone',$todo->id)}}")'/>
                    @endif
                    <!-- todo text -->
                    <small>{{$todo_user->name}}</small>

                    <span class="text">{{$todo->todo}}</span>
                    <!-- Emphasis label -->
                    <small class="label label-{{$color}}"><i class="fa fa-clock-o"></i> {{$todo->job_status}}</small>
                    <!-- General tools such as edit or delete-->
                    <div class="tools">
                      <a type="button" data-toggle="modal" class="todo-edit" data-todo-id="{{$todo->id}}" data-target="#modal-edit-{{$todo->id}}"><i class="fa fa-edit"></i></a>
                    </div>
                  </li>
                @endforeach

              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-default pull-right add-item" data-toggle="modal" data-target="#modal-default">
               <i class="fa fa-plus"></i> Add item
             </button>
            </div>
          </div>
          <!-- /.box -->
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-6">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#timeline" data-toggle="tab">Info Terupdate</a></li>
        </ul>
        <div class="tab-content">

          <div class="active tab-pane" id="timeline">
            <p><b>Kirim Informasi :</b></p>
            <form class="form-horizontal" method="post" action="{{ route('ProjectEdit',$project->id) }}"  enctype="multipart/form-data">
              <div class="form-group">
                <div class="col-sm-12">
                  <textarea name="description" rows="2" class="form-control" required></textarea>
                </div>
              </div>
              <div class="form-group">


                <div class="col-sm-6">
                  <label>Assign To</label>
                  <select class="form-control" class="" name="assign">
                    <option value="">--assign to --</option>
                  @foreach($teams as $n => $team)
                    <option value="{{$team->id}}">{{$team->name}}</option>
                  @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6">
                  <input type="file" name="bukti" class="form-control">
                </div>
              </div>
              {{csrf_field()}}
              <input type="hidden" name="project_id" value="{{$project->id}}">
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success pull-right">Submit</button>
                </div>
              </div>
            </form>

            <!-- The timeline -->

            <ul class="timeline timeline-inverse">
              @foreach($activities as $activity)
                <?php
                  $to = DB::table('cms_users')->where('id',$activity->user_id)->first();
                  $from = DB::table('cms_users')->where('id',$activity->from_id)->first();
                ?>
                <!-- timeline time label -->

                <li class="time-label">
                  <span class="bg-blue">
                    {{ date('d-m-Y',strtotime($activity->created_at))}}
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa fa-user bg-aqua"></i>

                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> {{ $activity->created_at ? $activity->created_at : '-' }}</span>

                    <h3 class="timeline-header"><a href="#">{{ ucwords($from->name) }}</a> </h3>

                    <div class="timeline-body">
                      @if($activity->image)
                        <img src="{{url('/')}}/uploads/{{$activity->image}}" alt="" width="80%">
                      @endif
                      <br>
                      {!! nl2br($activity->description) !!}

                    </div>
                    @if(!empty($activity->user_id))
                    <div class="timeline-footer">
                      for <strong>{{ucwords($to->name)}}</strong>
                    </div>
                    @endif
                  </div>
                </li>
                <!-- END timeline item -->
              @endforeach
                <li>
                  <i class="fa fa-clock-o bg-gray"></i>
                </li>
            </ul>
          </div>
          <!-- /.tab-pane -->

        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Things to do</h4>
          </div>
          <div class="modal-body">
            <form action="{{route('todoPost')}}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="projectName">Project Name:</label>
                <input type="text" class="form-control" name="projectName" placeholder="Maxlength 140" maxlength="140" value="{{$project->nama_project}}" readonly/>
              </div>
              <div class="form-group">
                <label for="projectTodo">Things to do:</label>
                <input type="text" class="form-control" name="projectToDo" placeholder="Max 255 Characters" maxlength="255" required/>
              </div>
              <div class="form-group">
                <label for="projectUser">PIC :</label>
                <select class="form-control" name="projectUser">
                  @foreach ($teams as $team)
                    <option value="{{$team->id}}">{{$team->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="projectUser">To do Status :</label>
                <select class="form-control" name="todoStatus">
                  <option value="new">new</option>
                  <option value="progress">progress</option>
                  <option value="check">check</option>
                  <option value="reject">reject</option>
                  <option value="revision">revision</option>
                  <option value="done">done</option>
                </select>
              </div>
              <div class="form-group">
                <label for="projectDeadline">Project Deadline:</label>
                <input type="date" class="form-control" name="projectDeadline" required/>
              </div>
              <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Detail Deadline:</label>
                  <div class="input-group">
                    <input type="text" name="projectDetailDeadline" class="form-control formadd-timepicker">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
              <input type="hidden" name="project_id" value="{{$project->id}}">
              <div class="form-group">
                <input type="submit" class="form-control btn-success" value="SUBMIT"/>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@foreach($todolist as $mtd)
  <?php
      $mtd_user = DB::table('cms_users')->where('id',$todo->user_id)->first();
      $deadlineDetail = date('H:i:s', strtotime($mtd->deadline));
  ?>
  
  <div class="modal fade" id="modal-edit-{{$mtd->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Things to do</h4>
          </div>
          <div class="modal-body">
            <form action="{{route('todoEdit',$mtd->id)}}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="projectName">Project Name:</label>
                <input type="text" class="form-control" value="{{$project->nama_project}}" readonly/>
              </div>
              <div class="form-group">
                <label for="projectTodo">Things to do:</label>
                <input type="text" class="form-control" value="{{$mtd->todo}}" readonly/>
              </div>
              <div class="form-group">
                <label for="projectUser">PIC :</label>
                <input type="text" class="form-control" placeholder="{{$mtd_user->name}}" readonly>
              </div>
              <div class="form-group">
                <label for="projectUser">To do Status :</label>
                <select class="form-control" name="todoStatus">
                  <option value="new" {{$mtd->todoStatus=="new" ? "selected" : ""}}>new</option>
                  <option value="progress" {{$mtd->todoStatus=="progress" ? "selected" : ""}}>progress</option>
                  <option value="check" {{$mtd->todoStatus=="check" ? "selected" : ""}}>check</option>
                  <option value="reject" {{$mtd->todoStatus=="reject" ? "selected" : ""}}>reject</option>
                  <option value="revision" {{$mtd->todoStatus=="revision" ? "selected" : ""}}>revision</option>
                  <option value="done" {{$mtd->todoStatus=="done" ? "selected" : ""}}>done</option>
                </select>
              </div>
              <div class="form-group">
                <label for="projectDeadline">Project Deadline:</label>
                <input type="date" class="form-control" name="projectDeadline"/>
              </div>
              <div class="bootstrap-timepicker">
                <div class="form-group">
                    <label>Detail Deadline:</label>
                    <div class="input-group">
                      <input type="text" name="projectDetailDeadline" class="form-control edit-timepicker-{{$mtd->id}}">
                      <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                      </div>
                    </div>
                    <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
              <input type="hidden" name="project_id" value="{{$project->id}}">
              <div class="form-group">
                <input type="submit" class="form-control btn-success" value="SUBMIT"/>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endforeach

@endsection

@section('jslineup')
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/knob/jquery.knob.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>

  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/dist/js/pages/dashboard.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.js"  type="text/javascript"></script>


@endsection

@section('jsonpage')

  <script type="text/javascript">

  //Timepicker
    $('.timepicker').timepicker({
      showInputs: true
    })

    $('.add-item').on('click',function(){
      $('.formadd-timepicker').timepicker({
        showInputs: true
      })
    });

    $('.todo-edit').on('click',function(){
      var todoId = $(this).attr('data-todo-id');
      $(`.edit-timepicker-${todoId}`).timepicker({
        showInputs: true
      })
    });

    function updateData(data){
      $.ajax({
        url: "{{route('PostUpdateListTodo')}}",
        type: 'POST',
        data: data ,
        success: function (data, textStatus, jqXHR) {
          // console.log('data',data)
          console.log('textStatus',textStatus)
        }
      });
    };

    var originPosition = 0;
    $( ".todo-list" ).sortable({
      change: function( event, ui ) {
        originPosition = ui.item.index();
      },
      update:function( event, ui ) {
        // console.log('update')
        // console.log('event',event);
        // console.log('ui',ui);
        // console.log('inedex',ui.item.index());
        // if(ui.item.index()){
          var target = ui.item[0];
          var data ={
            originPosition,
            updatePosition : ui.item.index(),
            id:$(target).attr('data-id'),
            project_id:$(target).attr('data-project_id'),
            user_id:$(target).attr('data-user_id')
          }
          // console.log('data',data);
          updateData(data);
        // }
        
      },
    });


  </script>

@endsection
