@extends("crudbooster::admin_template")

@section("content")
  <div class="row">
    @foreach ($projects as $project)
      <?php
        $user = DB::table('cms_users')->where('id',$project->user_id)->first();
       ?>
      <div class="col-md-3">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user">
          <!-- Add the bg color to the header using any of the bg-* classes -->
          <a href="/project/detail/{{$project->id}}">
          <div class="widget-user-header bg-black" style="background: url('{{url('/')}}/{{$project->project_img}}') center center;">
          </div>

          <div class="widget-user-image">
            <img class="img-circle" src="{{url('/')}}/{{$user->photo}}" alt="User Avatar">
          </div>
          <div class="box-footer">
            <p>{{$project->nama_project}}</p>
          </div>
          </a>
        </div>
        <!-- /.widget-user -->
      </div>
      <!-- /.col -->
    @endforeach
  </div>

@endsection
