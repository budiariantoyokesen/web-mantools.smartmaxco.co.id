<?php

namespace App\Http\Middleware;

use Closure;
use Crudbooster;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Crudbooster::myID()){
          return $next($request);
        }else{
          return redirect()->route('HomeToLogin');
        }

    }
}
